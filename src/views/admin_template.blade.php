<!DOCTYPE html>

<html lang="es">
<!-- begin::Head -->
<head>
    <meta charset="UTF-8">
    <title>{{ ($page_title)?Session::get('appname').': '.strip_tags($page_title):"Admin Area" }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name='generator' content='Pasaporte Nuevo León'/>
    <meta name='robots' content='noindex,nofollow'/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon"
          href="{{ CRUDBooster::getSetting('favicon')?asset(CRUDBooster::getSetting('favicon')):asset('vendor/crudbooster/assets/logo_crudbooster.png') }}">
    <!--begin::Web font -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script> -->
    <script src="{{ asset('vendor/crudbooster/webfont.js') }}"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <!--begin::Page Vendors -->
    <link href="{{ asset("vendor/crudbooster/metronic/vendors/custom/fullcalendar/fullcalendar.bundle.css") }}"
          rel="stylesheet" type="text/css"/>
    <!--end::Page Vendors -->
    <link href="{{ asset("vendor/crudbooster/metronic/vendors/base/vendors.bundle.css")}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset("vendor/crudbooster/metronic/demo/default/base/style.bundle.css")}}" rel="stylesheet"
          type="text/css"/>
    <!--end::Base Styles -->

    <!-- load css -->
    <style type="text/css">
        @if($style_css)
            {!! $style_css !!}
        @endif
    </style>
    @if($load_css)
        @foreach($load_css as $css)
            <link href="{{$css}}" rel="stylesheet" type="text/css"/>
        @endforeach
    @endif

    @stack('head')
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
@include('crudbooster::header')
<!-- END: Header -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
    @include('crudbooster::sidebar')
    <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <?php
                $module = CRUDBooster::getCurrentModule();
                ?>
                @if($module)
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h1 class="m-subheader__title m-subheader__title--separator">
                                <i class='{{$module->icon}}'></i> {{($page_title)?:$module->name}} &nbsp;&nbsp;

                                <!--START BUTTON -->

                                @if(CRUDBooster::getCurrentMethod() == 'getIndex')
                                    @if($button_show)
                                        <a href="{{ CRUDBooster::mainpath().'?'.http_build_query(Request::all()) }}"
                                           id='btn_show_data' class="btn btn-sm btn-primary"
                                           title="{{trans('crudbooster.action_show_data')}}">
                                            <i class="fa fa-table"></i> {{trans('crudbooster.action_show_data')}}
                                        </a>
                                    @endif

                                    @if($button_add && CRUDBooster::isCreate())
                                        <a href="{{ CRUDBooster::mainpath('add').'?return_url='.urlencode(Request::fullUrl()).'&parent_id='.g('parent_id').'&parent_field='.$parent_field }}"
                                           id='btn_add_new_data' class="btn btn-sm btn-success"
                                           title="{{trans('crudbooster.action_add_data')}}">
                                            <i class="fa fa-plus-circle"></i> {{trans('crudbooster.action_add_data')}}
                                        </a>
                                    @endif
                                @endif


                                @if($button_export && CRUDBooster::getCurrentMethod() == 'getIndex')
                                    <a href="javascript:void(0)" id='btn_export_data'
                                       data-url-parameter='{{$build_query}}' title='Export Data'
                                       class="btn btn-sm btn-primary btn-export-data">
                                        <i class="fa fa-upload"></i> {{trans("crudbooster.button_export")}}
                                    </a>
                                @endif

                                @if($button_import && CRUDBooster::getCurrentMethod() == 'getIndex')
                                    <a href="{{ CRUDBooster::mainpath('import-data') }}" id='btn_import_data'
                                       data-url-parameter='{{$build_query}}' title='Import Data'
                                       class="btn btn-sm btn-primary btn-import-data">
                                        <i class="fa fa-download"></i> {{trans("crudbooster.button_import")}}
                                    </a>
                                @endif

                            <!--ADD ACTIon-->
                                @if(count($index_button))

                                    @foreach($index_button as $ib)
                                        <a href='{{$ib["url"]}}' id='{{str_slug($ib["label"])}}'
                                           class='btn {{($ib['color'])?'btn-'.$ib['color']:'btn-primary'}} btn-sm'
                                           @if($ib['onClick']) onClick='return {{$ib["onClick"]}}' @endif
                                           @if($ib['onMouseOever']) onMouseOever='return {{$ib["onMouseOever"]}}' @endif
                                           @if($ib['onMoueseOut']) onMoueseOut='return {{$ib["onMoueseOut"]}}' @endif
                                           @if($ib['onKeyDown']) onKeyDown='return {{$ib["onKeyDown"]}}' @endif
                                           @if($ib['onLoad']) onLoad='return {{$ib["onLoad"]}}' @endif
                                        >
                                            <i class='{{$ib["icon"]}}'></i> {{$ib["label"]}}
                                        </a>
                                @endforeach
                            @endif
                            <!-- END BUTTON -->
                            </h1>

                            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                                <li class="m-nav__item m-nav__item--home">
                                    <a href="#" class="m-nav__link m-nav__link--icon">
                                        <i class="m-nav__link-icon fa fa-dashboard"></i>
                                    </a>
                                </li>
                                <li class="m-nav__item">
                                    <a href="{{CRUDBooster::adminPath()}}" class="m-nav__link">
											<span class="m-nav__link-text">
												{{ trans('crudbooster.home') }}
											</span>
                                    </a>
                                </li>
                                <li class="m-nav__separator">
                                    -
                                </li>
                                <li class="m-nav__item">
                                    <a class="m-nav__link">
                                        <span class="m-nav__link-text">
                                            {{$module->name}}
                                        </span>
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                @else
                    <h3>{{Session::get('appname')}}
                        <small>Information</small>
                    </h3>
                @endif
            </div>

            <!-- END: Subheader -->
            <div id='content_section' class="m-content">
                <!-- Main content -->
                @if(@$alerts)
                    @foreach(@$alerts as $alert)
                        <div class='callout callout-{{$alert[type]}}'>
                            {!! $alert['message'] !!}
                        </div>
                    @endforeach
                @endif


                @if (Session::get('message')!='')
                    <div class='alert alert-{{ Session::get("message_type") }}'>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4>
                            <i class="icon fa fa-info"></i> {{ trans("crudbooster.alert_".Session::get("message_type")) }}
                        </h4>
                        {!!Session::get('message')!!}
                    </div>
                @endif
            <!-- Your Page Content Here -->
                @yield('content')
            </div>
        </div>
        @if(CRUDBooster::getCurrentMethod() != 'getIndex')
        <!-- begin::Footer -->
            @include('crudbooster::footer')
        <!-- end::Footer -->
        @endif
    </div>
    @if(CRUDBooster::getCurrentMethod() == 'getIndex')
    <!-- begin::Footer -->
        @include('crudbooster::footer')
    <!-- end::Footer -->
    @endif
</div>
<!-- end:: Body -->
</div>
<!-- end:: Page -->
<!-- begin::Quick Sidebar -->
<!-- end::Quick Sidebar -->
<!-- begin::Scroll Top -->
<!-- begin::Quick Nav -->

@include('crudbooster::admin_template_plugins')

<!-- load js -->
@if($load_js)
    @foreach($load_js as $js)
        <script src="{{$js}}"></script>
    @endforeach
@endif
<script type="text/javascript">
    var site_url = "{{url('/')}}";
    @if($script_js)
        {!! $script_js !!}
    @endif
</script>

@stack('bottom')

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience -->

</body>
<!-- end::Body -->
</html>