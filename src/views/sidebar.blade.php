<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div
            id="m_ver_menu"
            class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
            data-menu-vertical="true"
            data-menu-scrollable="false" data-menu-dropdown-timeout="500"
    >
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <li class="m-menu__section">
                <h4 class="m-menu__section-text">
                    {{trans("crudbooster.menu_navigation")}}
                </h4>
                <i class="m-menu__section-icon flaticon-more-v3"></i>
            </li>
            <?php $dashboard = CRUDBooster::sidebarDashboard();?>
            @if($dashboard)
                <li data-id='{{$dashboard->id}}'
                    class="m-menu__item m-menu__item--{{ (Request::is(config('crudbooster.ADMIN_PATH'))) ? 'active' : '' }}"
                    aria-haspopup="true">
                    <a href="{{CRUDBooster::adminPath()}}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-line-graph"></i>
                        <span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												{{trans("crudbooster.text_dashboard")}}
											</span>
                                            <!--<span class="m-menu__link-badge">
                                                <span class="m-badge m-badge--danger">
                                                    2
                                                </span>
                                            </span>-->
										</span>
									</span>
                    </a>
                </li>
            @endif

            @foreach(CRUDBooster::sidebarMenu() as $menu)
                <li data-id='{{$menu->id}}'
                    class="m-menu__item {{(count($menu->children))?"m-menu__item--submenu":""}} {{((CRUDBooster::getCurrentMenuId()==$menu->id || count($menu->children) && CRUDBooster::getCurrentMenuId()==$menu->children->where("parent_id",$menu->id)->first()->id) && CRUDBooster::getCurrentDashboardId()!=$menu->id && !Request::is(config('crudbooster.ADMIN_PATH').'/settings*', config('crudbooster.ADMIN_PATH').'/settings*', config('crudbooster.ADMIN_PATH').'/users*', config('crudbooster.ADMIN_PATH').'/privileges*', config('crudbooster.ADMIN_PATH').'/menu_management*', config('crudbooster.ADMIN_PATH').'/module_generator*',config('crudbooster.ADMIN_PATH').'/statistic_builder*',config('crudbooster.ADMIN_PATH').'/api_generator*',config('crudbooster.ADMIN_PATH').'/email_templates*',config('crudbooster.ADMIN_PATH').'/logs*'))?"m-menu__item--open m-menu__item--active m-menu__item--expanded":""}}"
                    aria-haspopup="true" data-menu-submenu-toggle="hover">
                    <a href='{{ ($menu->is_broken)?"javascript:alert('".trans('crudbooster.controller_route_404')."')":$menu->url."?m=".$menu->id }}'
                       class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon {{$menu->icon}}"></i>
                        <span class="m-menu__link-text">
                            {{$menu->name}}
                        </span>
                        @if(count($menu->children))
                            <i class="m-menu__ver-arrow la la-angle-{{ trans("crudbooster.right") }}"></i>
                        @endif
                    </a>
                    @if(count($menu->children))
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													{{$menu->name}}
												</span>
											</span>
                                </li>
                                @foreach($menu->children as $child)
                                    <li data-id='{{$child->id}}' class='m-menu__item {{(CRUDBooster::getCurrentMenuId()==$child->id && CRUDBooster::getCurrentDashboardId()!=$child->id && !Request::is(config('crudbooster.ADMIN_PATH').'/settings*', config('crudbooster.ADMIN_PATH').'/settings*', config('crudbooster.ADMIN_PATH').'/users*', config('crudbooster.ADMIN_PATH').'/privileges*', config('crudbooster.ADMIN_PATH').'/menu_management*', config('crudbooster.ADMIN_PATH').'/module_generator*',config('crudbooster.ADMIN_PATH').'/statistic_builder*',config('crudbooster.ADMIN_PATH').'/api_generator*',config('crudbooster.ADMIN_PATH').'/email_templates*',config('crudbooster.ADMIN_PATH').'/logs*'))?"m-menu__item--active":""}}' aria-haspopup="true">
                                        <a href="{{ ($child->is_broken)?"javascript:alert('".trans('crudbooster.controller_route_404')."')":$child->url."?m=".$child->id }}"
                                           class="m-menu__link ">
                                            <span class="m-menu__link-text">
                                                        <i class='{{$child->icon}}'></i>
                                                        <span>{{$child->name}}</span>
                                                    </span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </li>

            @endforeach

            @if(CRUDBooster::isSuperadmin())
                <li class="m-menu__section">
                    <h4 class="m-menu__section-text">
                        {{ trans('crudbooster.SUPERADMIN') }}
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>
                <li class='m-menu__item m-menu__item--submenu m-menu__item--submenu {{ (Request::is(config('crudbooster.ADMIN_PATH').'/privileges*')) ? 'm-menu__item--open m-menu__item--expanded' : '' }}'>
                    <a href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon la la-key"></i>
                        <span class="m-menu__link-text">
                            {{ trans('crudbooster.Privileges_Roles') }}
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-{{ trans("crudbooster.right") }}"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													{{ trans('crudbooster.Privileges_Roles') }}
												</span>
											</span>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/privileges/add*')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("PrivilegesControllerGetAdd")}}' class="m-menu__link ">
                                    {{ $current_path }}
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-plus'></i>
                                        <span>{{ trans('crudbooster.Add_New_Privilege') }}</span>
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/privileges')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("PrivilegesControllerGetIndex")}}' class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-bars'></i>
                                        <span>{{ trans('crudbooster.List_Privilege') }}</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class='m-menu__item m-menu__item--submenu m-menu__item--submenu {{ (Request::is(config('crudbooster.ADMIN_PATH').'/users*')) ? 'm-menu__item--open m-menu__item--expanded' : '' }}'>
                    <a href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon la la-users"></i>
                        <span class="m-menu__link-text">
                            {{ trans('crudbooster.Users_Management') }}
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-{{ trans("crudbooster.right") }}"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													{{ trans('crudbooster.Users_Management') }}
												</span>
											</span>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/users/add*')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("AdminCmsUsersControllerGetAdd")}}' class="m-menu__link ">
                                    {{ $current_path }}
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-plus'></i>
                                        <span>{{ trans('crudbooster.add_user') }}</span>
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/users')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("AdminCmsUsersControllerGetIndex")}}' class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-bars'></i>
                                        <span>{{ trans('crudbooster.List_users') }}</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/menu_management*')) ? 'm-menu__item--active' : '' }}">
                    <a href="{{Route("MenusControllerGetIndex")}}?m=0" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon fa fa-bars"></i>
                        <span class="m-menu__link-text">
                            {{ trans('crudbooster.Menu_Management') }}
                        </span>
                    </a>
                </li>

                <li class='m-menu__item m-menu__item--submenu m-menu__item--submenu {{ (Request::is(config('crudbooster.ADMIN_PATH').'/users*')) ? 'm-menu__item--open m-menu__item--expanded' : '' }}'>
                    <a href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon fa fa-wrench"></i>
                        <span class="m-menu__link-text">
                            {{ trans('crudbooster.settings') }}
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-{{ trans("crudbooster.right") }}"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													{{ trans('crudbooster.settings') }}
												</span>
											</span>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/settings/add*')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("SettingsControllerGetAdd")}}?m=0' class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-plus'></i>
                                        <span>{{ trans('crudbooster.Add_New_Setting') }}</span>
                                    </span>
                                </a>
                            </li>
                            <?php
                            $groupSetting = DB::table('cms_settings')->groupby('group_setting')->pluck('group_setting');
                            foreach($groupSetting as $gs):
                            ?>
                            <li class="m-menu__item <?=($gs == Request::get('group'))?'m-menu__item--active':''?>">
                                <a href='{{route("SettingsControllerGetShow")}}?group={{urlencode($gs)}}&m=0' class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-wrench'></i>
                                        <span>{{ $gs }}</span>
                                    </span>
                                </a>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </li>
                <li class='m-menu__item m-menu__item--submenu m-menu__item--submenu {{ (Request::is(config('crudbooster.ADMIN_PATH').'/module_generator*')) ? 'm-menu__item--open m-menu__item--expanded' : '' }}'>
                    <a href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon fa fa-th"></i>
                        <span class="m-menu__link-text">
                            {{ trans('crudbooster.Module_Generator') }}
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-{{ trans("crudbooster.right") }}"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													{{ trans('crudbooster.Module_Generator') }}
												</span>
											</span>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/module_generator/step1')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("ModulsControllerGetStep1")}}?m=0' class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-plus'></i>
                                        <span>{{ trans('crudbooster.Add_New_Module') }}</span>
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/module_generator')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("ModulsControllerGetIndex")}}?m=0' class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-bars'></i>
                                        <span>{{ trans('crudbooster.List_Module') }}</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class='m-menu__item m-menu__item--submenu m-menu__item--submenu {{ (Request::is(config('crudbooster.ADMIN_PATH').'/statistic_builder*')) ? 'm-menu__item--open m-menu__item--expanded' : '' }}'>
                    <a href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon fa fa-dashboard"></i>
                        <span class="m-menu__link-text">
                            {{ trans('crudbooster.Statistic_Builder') }}
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-{{ trans("crudbooster.right") }}"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													{{ trans('crudbooster.Statistic_Builder') }}
												</span>
											</span>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/statistic_builder/add')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("StatisticBuilderControllerGetAdd")}}?m=0' class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-plus'></i>
                                        <span>{{ trans('crudbooster.Add_New_Statistic') }}</span>
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/statistic_builder')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("StatisticBuilderControllerGetIndex")}}?m=0' class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-bars'></i>
                                        <span>{{ trans('crudbooster.List_Statistic') }}</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class='m-menu__item m-menu__item--submenu m-menu__item--submenu {{ (Request::is(config('crudbooster.ADMIN_PATH').'/api_generator*')) ? 'm-menu__item--open m-menu__item--expanded' : '' }}'>
                    <a href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon fa fa-fire"></i>
                        <span class="m-menu__link-text">
                            {{ trans('crudbooster.API_Generator') }}
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-{{ trans("crudbooster.right") }}"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													{{ trans('crudbooster.API_Generator') }}
												</span>
											</span>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/api_generator/generator*')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("ApiCustomControllerGetGenerator")}}?m=0' class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-plus'></i>
                                        <span>{{ trans('crudbooster.Add_New_API') }}</span>
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/api_generator')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("ApiCustomControllerGetIndex")}}?m=0' class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-bars'></i>
                                        <span>{{ trans('crudbooster.list_API') }}</span>
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/api_generator/screet-key*')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("ApiCustomControllerGetScreetKey")}}?m=0' class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-bars'></i>
                                        <span>{{ trans('crudbooster.Generate_Screet_Key') }}</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class='m-menu__item m-menu__item--submenu m-menu__item--submenu {{ (Request::is(config('crudbooster.ADMIN_PATH').'/email_templates*')) ? 'm-menu__item--open m-menu__item--expanded' : '' }}'>
                    <a href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon fa fa-envelope-o"></i>
                        <span class="m-menu__link-text">
                            {{ trans('crudbooster.Email_Templates') }}
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-{{ trans("crudbooster.right") }}"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													{{ trans('crudbooster.Email_Templates') }}
												</span>
											</span>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/email_templates/add*')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("EmailTemplatesControllerGetAdd")}}?m=0' class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-plus'></i>
                                        <span>{{ trans('crudbooster.Add_New_Email') }}</span>
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/email_templates')) ? 'm-menu__item--active' : '' }}">
                                <a href='{{Route("EmailTemplatesControllerGetIndex")}}?m=0' class="m-menu__link ">
                                    <span class="m-menu__link-text">
                                        <i class='fa fa-bars'></i>
                                        <span>{{ trans('crudbooster.List_Email_Template') }}</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="m-menu__item {{ (Request::is(config('crudbooster.ADMIN_PATH').'/logs*')) ? 'm-menu__item--active' : '' }}">
                    <a href="{{Route("LogsControllerGetIndex")}}?m=0" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon fa fa-flag"></i>
                        <span class="m-menu__link-text">
                            {{ trans('crudbooster.Log_User_Access') }}
                        </span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>