@extends("crudbooster::admin_template")
@section("content")

    @push('head')
        <style>
            .select2-container--default .select2-selection--single {
                border-radius: 0px !important
            }

            .select2-container .select2-selection--single {
                height: 35px
            }
        </style>
    @endpush

    @push('bottom')
        <script src='<?php echo asset("vendor\crudbooster\metronic\demo\default\custom\components\forms\widgets\select2.js")?>'></script>
        <script>
            $(function () {
                $('.select2').select2();

            })
            $(function () {
                $('select[name=table]').change(function () {
                    var v = $(this).val().replace(".", "_");
                    $.get("{{CRUDBooster::mainpath('check-slug')}}/" + v, function (resp) {
                        if (resp.total == 0) {
                            $('input[name=path]').val(v);
                        } else {
                            v = v + resp.lastid;
                            $('input[name=path]').val(v);
                        }
                    })

                })
            })
        </script>
    @endpush

    <!--begin::Portlet-->
    <div class="m-portlet m-portlet--tabs m-portlet--primary m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Module Information
                        <small>
                            Setup Module
                        </small>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand  m-tabs-line--right m-tabs-line-danger"
                    role="tablist">
                    @if($id)
                        <li role="tab" class="nav-item m-tabs__item active">
                            <a class="nav-link m-tabs__link active"
                               href="{{Route('ModulsControllerGetStep1',['id'=>$id])}}">
                                <i class='fa fa-info'></i> Step 1 - Module Information
                            </a>
                        </li>
                        <li role="tab" class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" href="{{Route('ModulsControllerGetStep2',['id'=>$id])}}">
                                <i class='fa fa-table'></i> Step 2 - Table Display
                            </a>
                        </li>
                        <li role="tab" class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" href="{{Route('ModulsControllerGetStep3',['id'=>$id])}}">
                                <i class='fa fa-plus-square-o'></i> Step 3 - Form Display
                            </a>
                        </li>
                        <li role="tab" class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" href="{{Route('ModulsControllerGetStep4',['id'=>$id])}}">
                                <i class='fa fa-wrench'></i> Step 4 - Configuration
                            </a>
                        </li>
                    @else
                        <li role="tab" class="nav-item m-tabs__item active">
                            <a class="nav-link m-tabs__link active" href="#">
                                <i class='fa fa-info'></i> Step 1 - Module Information
                            </a>
                        </li>
                        <li role="tab" class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" href="#">
                                <i class='fa fa-table'></i> Step 2 - Table Display
                            </a>
                        </li>
                        <li role="tab" class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" href="#">
                                <i class='fa fa-plus-square-o'></i> Step 3 - Form Display
                            </a>
                        </li>
                        <li role="tab" class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" href="#">
                                <i class='fa fa-wrench'></i> Step 4 - Configuration
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
        <form method="post" action="{{Route('ModulsControllerPostStep2')}}">
            <div class="m-portlet__body">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="id" value="{{$row->id}}">
                <div class="form-group">
                    <label for="">Table</label>
                    <select name="table" id="table" required class="form-control select2" value="{{$row->table_name}}">
                        <option value="">{{trans('crudbooster.text_prefix_option')}} Table</option>
                        @foreach($tables_list as $table)

                            <option {{($table == $row->table_name)?"selected":""}} value="{{$table}}">{{$table}}</option>

                        @endforeach
                    </select>
                    <div class="help-block">
                        Do not use cms_* as prefix on your tables name
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Module Name</label>
                    <input type="text" class="form-control" required name="name" value="{{$row->name}}">
                </div>

                <div class="form-group">
                    <label for="">Icon</label>
                    <select name="icon" id="icon" required class="form-control select2">
                        @foreach($fontawesome as $f)
                            <option {{($row->icon == 'fa fa-'.$f)?"selected":""}} value="fa fa-{{$f}}">{{$f}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Module Slug</label>
                    <input type="text" class="form-control" required name="path" value="{{$row->path}}">
                    <div class="help-block">Please alpha numeric only, without space instead _ and or special
                        character
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <input checked type='checkbox' name='create_menu' value='1'/>
                        Also create menu for this module
                        <a href='#' title='If you check this, we will create the menu for this module'>(?)</a>
                    </div>
                    <div class="col-lg-5">
                        <a class='btn btn-default'
                           href='{{Route("ModulsControllerGetIndex")}}'> {{trans('crudbooster.button_back')}}</a>
                        <input type="submit" class="btn btn-primary" value="Step 2 &raquo;">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!--end::Portlet-->

    </div><!--END AUTO MARGIN-->

@endsection
