@push('bottom')
    <script>
        //== Class definition
        var FormRepeater = function() {

            //== Private functions
            var demo3 = function() {
                $('#form-group-{{$name}}').repeater({
                    initEmpty: false,

                    defaultValues: {
                        'text-input': 'foo'
                    },

                    show: function() {
                        $(this).slideDown();
                    },

                    hide: function(deleteElement) {
                        if(confirm('Are you sure you want to delete this element?')) {
                            $(this).slideUp(deleteElement);
                        }
                    }
                });
            }

            return {
                // public functions
                init: function() {
                    demo3();
                }
            };
        }();

        jQuery(document).ready(function() {
            FormRepeater.init();
        });

    </script>
@endpush