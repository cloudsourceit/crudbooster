@push('bottom')
    <script>
        jQuery(document).ready(function() {
            $('#m_timepicker_2, #m_timepicker_2_modal').timepicker({
                minuteStep: 1,
                showSeconds: true,
                showMeridian: false,
                snapToStep: true
            });
        });
    </script>
@endpush
<div class="{{$col_width?:'col-sm-12'}} form-group m-form__group row {{ ($errors->first($name))?"has-error":"" }}"
     id="form-group-{{$name}}" style="{{@$form['style']}}">
    <label class="col-form-label col-sm-3">
        {{$form['label']}}
        @if($required)
            <span class='text-danger' title='{!! trans('crudbooster.this_field_is_required') !!}'>*</span>
        @endif
    </label>
    <div id="m_repeater_1" class="col-sm-9">
        <div class="form-group  m-form__group row" id="m_repeater_1">
            <div data-repeater-list="" class="col-lg-12">
                <div data-repeater-item class="form-group m-form__group row align-items-center">
                    <div class="col-md-3">
                        <div class="m-form__group m-form__group--inline">
                            <div class="m-form__control">
                                <div class='input-group timepicker' id='m_timepicker_2'>
                                    <input type='text' class="form-control m-input" readonly placeholder="Select time"
                                           type="text"/>
                                    <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-clock-o"></i>
													</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-md-none m--margin-bottom-10"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="m-form__group m-form__group--inline">
                            <div class="m-form__control">
                                <input type="time" class="form-control m-input" placeholder="Enter contact number">
                            </div>
                        </div>
                        <div class="d-md-none m--margin-bottom-10"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="m-form__group m-form__group--inline">
                            <div class="m-form__control">
                                <input type="time" class="form-control m-input" placeholder="Enter contact number">
                            </div>
                        </div>
                        <div class="d-md-none m--margin-bottom-10"></div>
                    </div>
                    <div class="col-md-3">
                        <div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
                            <span>
                                <i class="la la-trash-o"></i>
                                <span>
                                    Delete
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-form__group form-group row">
            <label class="col-lg-2 col-form-label"></label>
            <div class="col-lg-4">
                <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
														<span>
															<i class="la la-plus"></i>
															<span>
																Add
															</span>
														</span>
                </div>
            </div>
        </div>
    </div>
</div>