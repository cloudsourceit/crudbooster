<?php $default = !empty($form['default']) ? $form['default'] : trans('crudbooster.text_prefix_option') . " " . $form['label']; ?>
@if($col_width == 'col-sm-10')
    <div class="col-sm-1"></div>
@endif
<div class="{{$col_width?:'col-sm-12'}} form-group m-form__group row {{ ($errors->first($name))?"has-error":"" }}"
     id="form-group-{{$name}}" style="{{@$form['style']}}">
    <div class="col-sm-12">
        <label class="col-form-label">
            {{$form['label']}}
            @if($required)
                <span class='text-danger' title='{!! trans('crudbooster.this_field_is_required') !!}'>*</span>
            @endif
        </label>
        <input id="{{$name}}" type="text"
               class="form-control m-form--state {{ ($errors->first($name))?"form-control-warning":"" }} m-input"
               name="{{$name}}" value="{{$value}}">
        @if($errors)
            <div class="form-control-feedback">
                {!! $errors->first($name)?"<i class='fa fa-info-circle'></i> ".$errors->first($name):"" !!}
            </div>
        @endif
        @if(@$form['help'])
            <span class="m-form__help">
            {{ @$form['help'] }}
        </span>
        @endif
    </div>
</div>
<script>
    $('#{{$name}}').selectize({
        plugins: ['remove_button'],
        options: [
            <?php
            if (@$form['datatable']):
                $raw = explode(",", $form['datatable']);
                $format = $form['datatable_format'];
                $table1 = $raw[0];
                $column1 = $raw[1];

                @$table2 = $raw[2];
                @$column2 = $raw[3];

                @$table3 = $raw[4];
                @$column3 = $raw[5];

                $selects_data = DB::table($table1)->select($table1 . "." . CB::pk($table1) . " AS id");

                if (\Schema::hasColumn($table1, 'deleted_at')) {
                    $selects_data->where($table1 . '.deleted_at', NULL);
                }

                if (@$form['datatable_where']) {
                    $selects_data->whereraw($form['datatable_where']);
                }

                if ($table1 && $column1) {
                    $orderby_table = $table1;
                    $orderby_column = $column1;
                }

                if ($table2 && $column2) {
                    $selects_data->join($table2, $table2 . '.' . CB::pk($table2), '=', $table1 . '.' . $column1);
                    $orderby_table = $table2;
                    $orderby_column = $column2;
                }

                if ($table3 && $column3) {
                    $selects_data->join($table3, $table3 . '.' . CB::pk($table3), '=', $table2 . '.' . $column2);
                    $orderby_table = $table3;
                    $orderby_column = $column3;
                }

                if ($format) {
                    $format = str_replace('&#039;', "'", $format);
                    $selects_data->addselect(DB::raw("CONCAT($format) as label"));
                    $selects_data = $selects_data->orderby(DB::raw("CONCAT($format)"), "asc")->get();
                } else {
                    $selects_data->addselect($orderby_table . '.' . $orderby_column . ' as label');
                    $selects_data = $selects_data->orderby($orderby_table . '.' . $orderby_column, "asc")->get();
                }


                foreach ($selects_data as $d) {
                    echo "{id: ".$d->id.", text: '".$d->label."'},";
                }
            endif;
            ?>
        ],
        labelField: 'text',
        valueField: 'id',
        searchField: ['text'],
        maxItems: 25,
        persist: false,
        create: true
    });
</script>