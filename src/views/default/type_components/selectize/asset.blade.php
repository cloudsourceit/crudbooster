@push('head')
    <link href="{{ asset("vendor/crudbooster/custom/selectize/css/normalize.css")}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset("vendor/crudbooster/custom/selectize/css/stylesheet.css")}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset("vendor/crudbooster/custom/selectize/css/selectize.default.css")}}" rel="stylesheet"
          type="text/css"/>
    <script src="{{ asset("vendor/crudbooster/custom/selectize/js/jquery.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("vendor/crudbooster/custom/selectize/js/jqueryui.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset("vendor/crudbooster/custom/selectize/js/standalone/selectize.min.js") }}"
            type="text/javascript"></script>

@endpush

