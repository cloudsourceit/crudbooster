<div class="{{$col_width?:'col-sm-12'}} form-group m-form__group row {{ ($errors->first($name))?"has-error":"" }}"
     id="form-group-{{$name}}" style="{{@$form['style']}}" >
        <div class="col-sm-12">
            <label class="col-form-label">
                {{$form['label']}}
                @if($required)
                    <span class='text-danger' title='{!! trans('crudbooster.this_field_is_required') !!}'>*</span>
                @endif
            </label>
            @push('bottom')
                <script>
                    $('#{{$name}}').timepicker({
                        showMeridian: false
                    });
                </script>
            @endpush
            <input type='text' title="{{$form['label']}}" class='form-control timepicker'
                   name="{{$name}}" id="{{$name}}"
                   readonly value="{{$value}}"/>

            @if($errors)
                <div class="form-control-feedback">
                    {!! $errors->first($name)?"<i class='fa fa-info-circle'></i> ".$errors->first($name):"" !!}
                </div>
            @endif
            @if(@$form['help'])
                <span class="m-form__help">
            {{ @$form['help'] }}
        </span>
            @endif
        </div>
    </div>
