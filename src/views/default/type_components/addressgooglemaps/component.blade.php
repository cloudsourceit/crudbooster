@push('bottom')
    <script>
        // Note: This example requires that you consent to location sharing when
        // prompted by your browser. If you see the error "The Geolocation service
        // failed.", it means you probably did not give permission for the browser to
        // locate you.
        var map, infoWindow;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 6
            });
            infoWindow = new google.maps.InfoWindow;

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    infoWindow.setPosition(pos);
                    infoWindow.setContent('Location found.');
                    infoWindow.open(map);
                    map.setCenter(pos);
                }, function() {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8YhtYFvpuihVYmZhY2sTpPfSki9z1t6I&callback=initMap">
    </script>
@endpush

<div class="{{$col_width?:'col-sm-12'}} form-group m-form__group row {{ ($errors->first($name))?"has-danger":"" }}">
    <label class="form-control-label col-sm-3" for="{{$name}}">
        {{$form['label']}}
        @if($required)
            <span class='text-danger' title='{!! trans('crudbooster.this_field_is_required') !!}'>*</span>
        @endif
    </label>
    <div class="col-sm-9">
        <input type='text' title="{{$form['label']}}"
               {{$required}} {{$readonly}} {!!$placeholder!!} {{$disabled}} {{$validation['max']?"maxlength=".$validation['max']:""}} class="form-control m-form--state {{ ($errors->first($name))?"form-control-warning":"" }} m-input"
               name="{{$name}}" id="{{$name}}" value='{{$value}}'>
        @if($errors)
            <div class="form-control-feedback">
                {!! $errors->first($name)?"<i class='fa fa-info-circle'></i> ".$errors->first($name):"" !!}
            </div>
        @endif
        @if(@$form['help'])
            <span class="m-form__help">
            {{ @$form['help'] }}
        </span>
        @endif
    </div>
    <div class="w-100"><br></div>
    <div class="col-sm-9">
        <div id="map" style="height:300px;"></div>
    </div>
    <div class="col-sm-3 row">
        <div class="col-sm-12 form-group m-form__group row">
            <label class="form-control-label col-sm-12" for="{{$name}}">
                Latitud
                @if($required)
                    <span class='text-danger' title='{!! trans('crudbooster.this_field_is_required') !!}'>*</span>
                @endif
            </label>
            <div class="col-sm-12">
                <input type='text' title="{{$form['label']}}"
                       {{$required}} {{$readonly}} {!!$placeholder!!} {{$disabled}} {{$validation['max']?"maxlength=".$validation['max']:""}} class="form-control m-form--state {{ ($errors->first($name))?"form-control-warning":"" }} m-input"
                       name="{{$name}}" id="{{$name}}" value='{{$value}}'>
                @if($errors)
                    <div class="form-control-feedback">
                        {!! $errors->first($name)?"<i class='fa fa-info-circle'></i> ".$errors->first($name):"" !!}
                    </div>
                @endif
                @if(@$form['help'])
                    <span class="m-form__help">
            {{ @$form['help'] }}
        </span>
                @endif
            </div>
        </div>
    </div>
</div>