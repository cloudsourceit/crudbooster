@if($col_width == 'col-sm-10')
    <div class="col-sm-1"></div>
@endif
<div class="{{$col_width?:'col-sm-12'}} form-group m-form__group row {{ ($errors->first($name))?"has-error":"" }}"
     id="form-group-{{$name}}" style="{{@$form['style']}}">
    <div class="col-sm-12">
        <label class="form-control-label" for="{{$name}}">
            {{$form['label']}}
            @if($required)
                <span class='text-danger' title='{!! trans('crudbooster.this_field_is_required') !!}'>*</span>
            @endif
        </label>
        <div class="input-group m-input-group">
            <span class="input-group-text" id="basic-addon1">
                <i class="fa fa-envelope"></i>
            </span>
            <input type="email" name="{{$name}}" style="display: none">
            <input type="email" title="{{$form['label']}}"
                   {{$required}} {{$readonly}} {!!$placeholder!!} {{$disabled}} {{$validation['max']?"maxlength=".$validation['max']:""}} class="form-control {{ ($errors->first($name))?"form-control-danger":"" }} m-input"
                   name="{{$name}}" id="{{$name}}" value='{{$value}}'>
        </div>
        @if($errors)
            <div class="form-control-feedback">
                {!! $errors->first($name)?"<i class='fa fa-info-circle'></i> ".$errors->first($name):"" !!}
            </div>
        @endif
        @if(@$form['help'])
            <span class="m-form__help">
            {{ @$form['help'] }}
        </span>
        @endif
    </div>
</div>
@if($col_width == 'col-sm-10')
    <div class="col-sm-1"></div>
@endif