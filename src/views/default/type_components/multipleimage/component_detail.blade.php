<div style="height: 200px;">
    <ul id="moodular" style="height: 200px;overflow-y: scroll;">
        @foreach ($row->{$name} as $aPhoto)
            <li style="background-color:#fff;height: 100%;"><p style="background-size: contain;background-repeat:no-repeat;background-image: url({{url($aPhoto["url"])}}); height: 100%"></p></li>
        @endforeach
    </ul>
</div>
