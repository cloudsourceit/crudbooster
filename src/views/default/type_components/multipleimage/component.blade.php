@push('bottom')
    <script>
        function readURL(input) {
            var index = ($(input).data('index'));
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(input).parent().children('.newImage')
                        .attr('src', e.target.result)
                        .height(120);
                };
                reader.readAsDataURL(input.files[0]);

                $('.addPh').show();
                $('.addPh').last().clone().insertBefore($('#addPhoto')).hide();
                ++index;
                $('.addPh').last().children('.photo_new').data('index', index).attr('name', '{{$name}}[' + index + ']');
                $('.addPh').last().wrap('<form>').closest('form').get(0).reset();
                $('.addPh').last().unwrap();
            }
        }

        function readURLFile(input) {
            var index = ($(input).data('index'));
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var fileName = $(input).val().replace(/^.*[\\\/]/, '');
                    $(input).parent().children('.item-file').children('span').html(fileName);
                };
                reader.readAsDataURL(input.files[0]);
                $('.addFl').show();
                $('.addFl').last().clone().insertBefore($('#addFile')).hide();
                ++index;
                $('.addFl').last().children('.file_new').data('index', index).attr('name', 'Files[' + index + ']');
                $('.addFl').last().wrap('<form>').closest('form').get(0).reset();
                $('.addFl').last().unwrap();
            }
        }

        $(document).ready(function () {
            $('#addImage-{{$name}}').click(function () {
                $(this).parent().parent().find('.photo_new').last().click();
                $(this).parent().parent().find(".trash-cover").unbind("click");
                $(this).parent().parent().find('.trash-cover').click(function () {
                    if ($(this.parentElement).data('itemId') != undefined) {
                        var valueDeleted = $(this).parents('.images-loaded').find('.images-deleted').val();
                        if (valueDeleted == "") {
                            valueDeleted = $(this.parentElement).data('itemId');
                        } else {
                            valueDeleted += "," + $(this.parentElement).data('itemId');
                        }
                        $(this).parents('.images-loaded').find('.images-deleted').val(valueDeleted);
                    }
                    this.parentElement.parentElement.removeChild(this.parentElement);
                });
            });
            @if(CRUDBooster::getCurrentMethod()=="getEdit")
            $("#addImage-{{$name}}").parent().parent().find('.trash-cover').click(function () {
                if ($(this.parentElement).data('itemId') != undefined) {
                    var valueDeleted = $(this).parents('.images-loaded').find('.images-deleted').val();
                    if (valueDeleted == "") {
                        valueDeleted = $(this.parentElement).data('itemId');
                    } else {
                        valueDeleted += "," + $(this.parentElement).data('itemId');
                    }
                    $(this).parents('.images-loaded').find('.images-deleted').val(valueDeleted);
                }
                this.parentElement.parentElement.removeChild(this.parentElement);
            });
            @endif
        });
    </script>
    <!--script>
        debugger;
        $(function () {
// Multiple images preview in browser
            var cont = 0;
            var imagesPreview = function (input, placeToInsertImagePreview) {
                if (input.files) {
                    var filesAmount = input.files.length;

                    for (i = 0; i < filesAmount; i++) {
                        if(document.querySelectorAll('#image-container-{{$name}}').length > 0){
                            $('#gallery-photo-add').attr("required","false");
                        }
                        if (document.querySelectorAll('#image-container-{{$name}}').length < 8) {
                            cont = cont + 1;
                            var reader = new FileReader();
                            reader.onload = function (event) {
                                if (document.querySelectorAll('#image-container-{{$name}}').length == 4) {
                                    $($.parseHTML('<div class="col-sm-3 h-50" id="image-container-{{$name}}-' + cont + '">')).appendTo(placeToInsertImagePreview).html('<input name="{{$name}}[]" type="file" id="form-input-{{$name}}-' + cont + '" style="display: none;"/><img id="image-container-{{$name}}" class="img-thumbnail" src="' + event.target.result + '"><a href="javascript:void(0);" onclick="deleteImage(\'image-container-{{$name}}-' + cont + '\')" class="btn btn-danger m-btn m-btn--icon"><span><i class="fa fa-trash"></i><span>Eliminar</span></span></a><div class="{{$name}} w-100"><br><br></div>').appendTo(placeToInsertImagePreview);
                                } else if (document.querySelectorAll('#image-container-{{$name}}').length < 4 && cont > 4) {
                                    $('.{{$name}}').remove();
                                    $($.parseHTML('<div class="col-sm-3 h-50" id="image-container-{{$name}}-' + cont + '">')).appendTo(placeToInsertImagePreview).html('<input name="{{$name}}[]" type="file" id="form-input-{{$name}}" style="display: none;"/><img id="image-container-{{$name}}" class="img-thumbnail" src="' + event.target.result + '"><a href="javascript:void(0);" onclick="deleteImage(\'image-container-{{$name}}-' + cont + '\')" class="btn btn-danger m-btn m-btn--icon"><span><i class="fa fa-trash"></i><span>Eliminar</span></span></a>').appendTo(placeToInsertImagePreview);
                                }else{
                                    $($.parseHTML('<div class="col-sm-3 h-50" id="image-container-{{$name}}-' + cont + '">')).appendTo(placeToInsertImagePreview).html('<input name="{{$name}}[]" type="file" id="form-input-{{$name}}-' + cont + '" style="display: none;"/><img id="image-container-{{$name}}" class="img-thumbnail" src="' + event.target.result + '"><a href="javascript:void(0);" onclick="deleteImage(\'image-container-{{$name}}-' + cont + '\')" class="btn btn-danger m-btn m-btn--icon"><span><i class="fa fa-trash"></i><span>Eliminar</span></span></a>').appendTo(placeToInsertImagePreview);
                                }
                            };
                            reader.readAsDataURL(input.files[i]);
                            $('#imageview-{{$name}}-default').attr('onclick', " $('#form-input-{{$name}}-" + cont + "').click();");

                        } else {
                            alert("Solo pueden cargar 8 imagenes.");
                        }
                    }
                }
            };

            $('#gallery-photo-add').on('change', function () {
                imagesPreview(this, 'div.gallery');
            });
        });

        function deleteImage(id_image) {
            $('#' + id_image).remove();
            $('.{{$name}}').remove();
            $('<div class="{{$name}} w-100"><br></div>').insertAfter(document.querySelectorAll('#image-container-{{$name}}')[3].parentElement);
            if(document.querySelectorAll('#image-container-{{$name}}').length == 0){
                $('#gallery-photo-add').attr("required","true");
            }
        }
    </script-->
@endpush
@if($col_width == 'col-sm-10')
    <div class="col-sm-1"></div>
@endif
<div class="{{$col_width?:'col-sm-12'}} form-group m-form__group row {{ ($errors->first($name))?"has-error":"" }}"
     id="form-group-{{$name}}" style="{{@$form['style']}}">
    <div class="col-sm-12">
        <label class="form-control-label" for="{{$name}}">
            {{$form['label']}}
            @if($required)
                <span class='text-danger' title='{!! trans('crudbooster.this_field_is_required') !!}'>*</span>
            @endif
        </label>
        <div class="multiimaga-container">
            <div class="button-image-container">
                <button id="addImage-{{$name}}" type="button" class="btn btn-default">Add Image</button>
            </div>
            <div class="images-loaded">
                <input class="images-deleted" name="{{$name}}-deleted" style="display: none">
                @if(isset($row))
                    @foreach ($row->{$name} as $aPhoto)
                        <div class="itemPhotoThumbnail addPh" data-item-id="{{$aPhoto["id"]}}">
                            <div class="trash-cover"><i class="fa fa-trash" aria-hidden="true"></i></div>
                            <img class="newImage" src="{{$aPhoto["url"]}}"/>
                        </div>
                    @endforeach
                @endif
                <div class="itemPhotoThumbnail addPh" style="display: none;">
                    <div class="trash-cover"><i class="fa fa-trash" aria-hidden="true"></i></div>
                    <img class="newImage" src="#"/>
                    <input type="file" name="{{$name}}[0]" class="photo_new" style="display: none;" accept="image/*"
                           onchange="readURL(this);" data-index="0"/>
                </div>
                <div id="addPhoto" class="itemPhotoThumbnail"></div>

            <!--div class="col-sm-9">
        <input type="file" name="{{$name}}[]" multiple id="gallery-photo-add" style="display: none;">
        <img id="imageview-{{$name}}-default" onclick="$('#gallery-photo-add').click();" class="img-responsive img-fluid"
             src="{{ asset("vendor/crudbooster/metronic/demo/default/media/img/image-default.png") }}"
             style="width: 15%; cursor: pointer;">
        <br>
        <span class="m-form__help">
            Subir imagen
        </span>
    </div-->
                <div class="w-100"><br></div>
                <div class="gallery offset-1 col-sm-10 row"></div>
            </div>
        </div>
    </div>
</div>
