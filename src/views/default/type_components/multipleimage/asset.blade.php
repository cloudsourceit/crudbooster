@push('head')
    <style>
        .fa-trash{
            font-size: 30px;
        }
        .itemPhotoThumbnail{
            position: relative;
            background: white;
            float: left;
            margin-right: 10px;
            overflow: hidden;
            margin-bottom: 10px;
        }
        #addPhoto {
            cursor: pointer;
        }
        .button-image-container{
            margin-bottom: 20px;
        }
        .newImage{
            height: 120px;
            border: solid 1px #772020;
        }
        .trash-cover{
            opacity: 0;
            position: absolute;
            width: 100%;
            height: 100%;
            background-color: #7773734f;
            color: #fff;
            font-size: 50px;
            text-align: center;
            transition: opacity .5s ease-in-out;
        }
        .itemPhotoThumbnail:hover .trash-cover{
            opacity: 1;
        }
        .delete-item-file,.item-file{
            display: inline-block;
        }
        .item-file{
            margin-left: 10px;
        }
        .item-file span{
            margin-left: 10px;
        }
        #moodular {
            overflow: hidden;
        }
        #moodular, #moodular li {
            margin: 0;
            padding: 0;
            list-style: none;
            width: 100%;
        }
    </style>
@endpush