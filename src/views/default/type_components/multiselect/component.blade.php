@if($col_width == 'col-sm-10')
    <div class="col-sm-1"></div>
@endif
<div class="{{$col_width?:'col-sm-12'}} form-group m-form__group row {{ ($errors->first($name))?"has-error":"" }}"
     id="form-group-{{$name}}" style="{{@$form['style']}}">
    <div class="col-sm-12">
        <label class="col-form-label">
            {{$form['label']}}
            @if($required)
                <span class='text-danger' title='{!! trans('crudbooster.this_field_is_required') !!}'>*</span>
            @endif
        </label>
        <select class="form-control m-select2" multiple="multiple" id="{{$name}}" data-value='{{$value}}'
                {{$required}} {!!$placeholder!!} {{$readonly}} {{$disabled}} name="{{$name}}[]">
            <?php

            if (!$form['parent_select']) {
                if (@$form['dataquery']):

                    $query = DB::select(DB::raw($form['dataquery']));
                    if ($query) {
                        foreach ($query as $q) {
                            $selected = ($value == $q->value) ? "selected" : "";
                            echo "<option $selected value='$q->value'>$q->label</option>";
                        }
                    }

                endif;

                if (@$form['dataenum']):
                    $dataenum = $form['dataenum'];
                    $dataenum = (is_array($dataenum)) ? $dataenum : explode(";", $dataenum);

                    foreach ($dataenum as $d) {

                        $val = $lab = '';
                        if (strpos($d, '|') !== FALSE) {
                            $draw = explode("|", $d);
                            $val = $draw[0];
                            $lab = $draw[1];
                        } else {
                            $val = $lab = $d;
                        }

                        $select = ($value == $val) ? "selected" : "";

                        echo "<option $select value='$val'>$lab</option>";
                    }
                endif;
                if (@$form['datatable']):
                    $raw = explode(",", $form['datatable']);
                    $format = $form['datatable_format'];
                    $table1 = $raw[0];
                    $column1 = $raw[1];

                    @$table2 = $raw[2];
                    @$column2 = $raw[3];

                    @$table3 = $raw[4];
                    @$column3 = $raw[5];

                    $selects_data = DB::table($table1)->select($table1 . "." . CB::pk($table1) . " AS id");

                    if (\Schema::hasColumn($table1, 'deleted_at')) {
                        $selects_data->where($table1 . '.deleted_at', NULL);
                    }

                    if (@$form['datatable_where']) {
                        $selects_data->whereraw($form['datatable_where']);
                    }

                    if ($table1 && $column1) {
                        $orderby_table = $table1;
                        $orderby_column = $column1;
                    }

                    if ($table2 && $column2) {
                        $selects_data->join($table2, $table2 . '.' . CB::pk($table2), '=', $table1 . '.' . $column1);
                        $orderby_table = $table2;
                        $orderby_column = $column2;
                    }

                    if ($table3 && $column3) {
                        $selects_data->join($table3, $table3 . '.' . CB::pk($table3), '=', $table2 . '.' . $column2);
                        $orderby_table = $table3;
                        $orderby_column = $column3;
                    }

                    if ($format) {
                        $format = str_replace('&#039;', "'", $format);
                        $selects_data->addselect(DB::raw("CONCAT($format) as label"));
                        $selects_data = $selects_data->orderby(DB::raw("CONCAT($format)"), "asc")->get();
                    } else {
                        $selects_data->addselect($orderby_table . '.' . $orderby_column . ' as label');
                        $selects_data = $selects_data->orderby($orderby_table . '.' . $orderby_column, "asc")->get();
                    }
                    foreach ($selects_data as $d) {
                        $val = $d->id;
                        $datas = explode(',', $row->$name);
                        foreach ($datas AS $value){
                            if($value == $val){
                                $select = "selected";
                                break;
                            }else{
                                $select = "";
                            }
                        }
                        echo "<option $select value='$val'>" . $d->label . "</option>";
                    }

                endif;
            } //end if not parent select
            ?>
        </select>
    </div>
</div>
@if($col_width == 'col-sm-10')
    <div class="col-sm-1"></div>
@endif
@push('bottom')
    <script>
        //== Initialization
        $('#{{$name}}').select2({
            placeholder: "Select a {{$name}}",
            @if(isset($form['max']))
                maximumSelectionLength: {{$form['max']}}
            @endif
        });
    </script>
@endpush