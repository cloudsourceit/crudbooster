<?php
if ($form['datatable']) {
    $datatable = explode(',', $form['datatable']);
    $table = $datatable[0];
    $field = $datatable[1];
    $tableRel = explode(',', $form['table_master']);


    if (isset($form['table_master'])) {
        $data = DB::table($tableRel[0])->select($tableRel[2] . ' AS value')->where($tableRel[1], $id)->get();

        if (count($data) > 0) {
            foreach ($data AS $value) {
                $v = CRUDBooster::first($table, [CRUDBooster::pk($table) => $value->value])->$field;
                echo "<span class='m-badge m-badge--success m-badge--wide'>$v</span> ";
            }
        } else {
            echo "";
        }
    } else {
        echo "";
    }
}
if ($form['dataquery']) {
    $dataquery = $form['dataquery'];
    $query = DB::select(DB::raw($dataquery));
    if ($query) {
        foreach ($query as $q) {
            if ($q->value == $value) {
                echo $q->label;
                break;
            }
        }
    }
}
if ($form['dataenum']) {
    echo $value;
}
?>