<div class="button-image-container"><button id="addImage" type="button" class="btn btn-default">Add Image</button></div>
<div class="itemPhotoThumbnail addPh" style="display: none;" >
    <div class="trash-cover"><i class="fa fa-trash" aria-hidden="true"></i></div>
    <img class="newImage" src="#" />
    <input type="file" name="Images[0]" class="photo_new" style="display: none;" accept="image/*" onchange="readURL(this);" data-index="0" />
</div>
<div id="addPhoto" class="itemPhotoThumbnail"></div>