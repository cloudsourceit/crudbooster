@push('head')
    <link rel='stylesheet' href='<?php echo asset("vendor/crudbooster/custom/imagenpicker/image-picker.css")?>'/>
@endpush
@push('bottom')
    <script src='<?php echo asset("vendor/crudbooster/custom/imagenpicker/image-picker.min.js")?>'></script>
    <script>
        $("#{{$name}}").imagepicker({hide_select : true, show_label  : true});
    </script>
@endpush
