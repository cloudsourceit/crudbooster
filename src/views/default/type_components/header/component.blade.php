@push('head')
    <style type="text/css">
        .header-title {
            cursor: pointer;
        }
    </style>
@endpush

@if($form['position'] == "start")
    <div id="head-{{$name}}" data-toggle="collapse" data-target="#{{$name}}" aria-expanded="true" aria-controls="{{$name}}"
         class='{{$col_width?:'col-sm-12'}} form-group m-form__group row header-title'
         style="{{@$form['style']}}">
        <h4>
            <strong><i class='{{$form['icon']?:"fa fa-check-square-o"}}'></i> {{$form['label']}}</strong>
            <span class='pull-right icon'><i class='fa fa-minus-square-o'></i></span>
        </h4>
    </div>
    <div id="{{$name}}" class="collapse col-sm-12 row">
        <div class="col-sm-12 row">
            <br>
@else
        </div>
    </div>
@endif