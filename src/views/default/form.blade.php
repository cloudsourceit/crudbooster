@extends('crudbooster::admin_template')
@section('content')

    <div>

        @if(CRUDBooster::getCurrentMethod() != 'getProfile' && $button_cancel)
            @if(g('return_url'))
                <p><a title='Return' href='{{g("return_url")}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}
                    </a></p>
            @else
                <p><a title='Main Module' href='{{CRUDBooster::mainpath()}}'><i class='fa fa-chevron-circle-left '></i>
                        &nbsp; {{trans("crudbooster.form_back_to_list",['module'=>CRUDBooster::getCurrentModule()->name])}}
                    </a></p>
            @endif
        @endif
        @if(CRUDBooster::getCurrentMethod() != 'getProfile')
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="{{CRUDBooster::getCurrentModule()->icon}}"></i>
                        </span>
                            <h3 class="m-portlet__head-text">
                                {!! $page_title or "Page Title" !!}
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="" data-portlet-tool="Limpiar campos"
                                   class="m-portlet__nav-link m-portlet__nav-link--icon">
                                    <i class="la la-recycle"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <?php
                $action = (@$row) ? CRUDBooster::mainpath("edit-save/$id") : CRUDBooster::mainpath("add-save");
                $return_url = ($return_url) ?: g('return_url');
                ?>

                <form class='m-form m-form--state m-form--label-align-right' method='post' id="form"
                      enctype="multipart/form-data" action='{{$action}}'>

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
                    <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
                    <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                    @if($hide_form)
                        <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                    @endif
                    <div class="m-portlet__body row" id="parent-form-area">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-10 row">
                            <div class="w-100"></div>
                            @if($command == 'detail')
                                @include("crudbooster::default.form_detail")
                            @else
                                @include("crudbooster::default.form_body")
                            @endif
                        </div>
                        <div class="col-sm-1"></div>
                    </div><!-- /.m-portlet__body -->

                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                                @if($button_cancel && CRUDBooster::getCurrentMethod() != 'getDetail')
                                    @if(g('return_url'))
                                        <a href='{{g("return_url")}}' class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}
                                        </a>
                                    @else
                                        <a href='{{CRUDBooster::mainpath("?".http_build_query(@$_GET)) }}'
                                           class='btn btn-default'><i
                                                    class='fa fa-chevron-circle-left'></i> {{trans("crudbooster.button_back")}}
                                        </a>
                                    @endif
                                @endif
                                @if(CRUDBooster::isCreate() || CRUDBooster::isUpdate())

                                    @if(CRUDBooster::isCreate() && $button_addmore==TRUE && $command == 'add')
                                        <input type="submit" name="submit"
                                               value='{{trans("crudbooster.button_save_more")}}'
                                               class='btn btn-success'>
                                    @endif

                                    @if($button_save && $command != 'detail')
                                        <input type="submit" name="submit" value='{{trans("crudbooster.button_save")}}'
                                               class='btn btn-success'>
                                    @endif

                                @endif
                            </div>
                        </div>

                    </div><!-- /.box-footer-->
                </form>

            </div>

        @else
                <?php
                $action = (@$row) ? CRUDBooster::mainpath("edit-save/".CRUDBooster::myId()."") : CRUDBooster::mainpath("add-save");
                $return_url = ($return_url) ?: g('return_url');
                ?>
            <form class='' method='post' id="form"
                  enctype="multipart/form-data" action='{{$action}}'>

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type='hidden' name='return_url' value='{{ @$return_url }}'/>
                <input type='hidden' name='ref_mainpath' value='{{ CRUDBooster::mainpath() }}'/>
                <input type='hidden' name='ref_parameter' value='{{urldecode(http_build_query(@$_GET))}}'/>
                @if($hide_form)
                    <input type="hidden" name="hide_form" value='{!! serialize($hide_form) !!}'>
                @endif
                <div class="row">
                    <div class="col-xl-3 col-lg-4">
                        <div class="m-portlet m-portlet--full-height  ">
                            <div class="m-portlet__body">
                                <div class="m-card-profile">
                                    <div class="m-card-profile__title m--hide">
                                        Mi Perfil
                                    </div>
                                    <div class="m-card-profile__pic">
                                        <div class="m-card-profile__pic-wrapper">
                                            <img src="{{ CRUDBooster::myPhoto() }}" alt=""/>
                                        </div>
                                    </div>
                                    <div class="m-card-profile__details">
                                    <span class="m-card-profile__name">
                                        {{ CRUDBooster::myName() }}
                                    </span>
                                        <a href="" class="m-card-profile__email m-link">
                                            {{ CRUDBooster::myPrivilegeName() }}
                                        </a>
                                    </div>
                                </div>
                                <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                                    <li class="m-nav__separator m-nav__separator--fit"></li>
                                    <li class="m-nav__section m--hide">
												<span class="m-nav__section-text">
													Section
												</span>
                                    </li>
                                    <li class="m-nav__item">
                                        <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                            <i class="m-nav__link-icon flaticon-profile-1"></i>
                                            <span class="m-nav__link-title">
														<span class="m-nav__link-wrap">
															<span class="m-nav__link-text">
                                                                Mi Perfil
															</span>
															<!--<span class="m-nav__link-badge">
																<span class="m-badge m-badge--success">
																	2
																</span>
															</span>-->
														</span>
													</span>
                                        </a>
                                    </li>
                                    <!--<li class="m-nav__item">
                                        <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                            <i class="m-nav__link-icon flaticon-share"></i>
                                            <span class="m-nav__link-text">
														Activity
													</span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                            <i class="m-nav__link-icon flaticon-chat-1"></i>
                                            <span class="m-nav__link-text">
														Messages
													</span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                            <i class="m-nav__link-icon flaticon-graphic-2"></i>
                                            <span class="m-nav__link-text">
														Sales
													</span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                            <i class="m-nav__link-icon flaticon-time-3"></i>
                                            <span class="m-nav__link-text">
														Events
													</span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                            <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                            <span class="m-nav__link-text">
														Support
													</span>
                                        </a>
                                    </li>-->
                                </ul>
                                <div class="m-portlet__body-separator"></div>
                                <div class="m-widget1 m-widget1--paddingless">
                                    <!--<div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                    Member Profit
                                                </h3>
                                                <span class="m-widget1__desc">
															Awerage Weekly Profit
														</span>
                                            </div>
                                            <div class="col m--align-right">
														<span class="m-widget1__number m--font-brand">
															+$17,800
														</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                    Orders
                                                </h3>
                                                <span class="m-widget1__desc">
															Weekly Customer Orders
														</span>
                                            </div>
                                            <div class="col m--align-right">
														<span class="m-widget1__number m--font-danger">
															+1,800
														</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                    Issue Reports
                                                </h3>
                                                <span class="m-widget1__desc">
															System bugs and issues
														</span>
                                            </div>
                                            <div class="col m--align-right">
														<span class="m-widget1__number m--font-success">
															-27,49%
														</span>
                                            </div>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-8">
                        <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-tools">
                                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                                        role="tablist">
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link active" data-toggle="tab"
                                               href="#m_user_profile_tab_1" role="tab">
                                                <i class="flaticon-share m--hide"></i>
                                                Actualizar Perfil
                                            </a>
                                        </li>
                                        <!--<li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2"
                                               role="tab">
                                                Messages
                                            </a>
                                        </li>
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_3"
                                               role="tab">
                                                Settings
                                            </a>
                                        </li>-->
                                    </ul>
                                </div>
                                <!--<div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item m-portlet__nav-item--last">
                                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                                                 data-dropdown-toggle="hover" aria-expanded="true">
                                                <a href="#"
                                                   class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                                    <i class="la la-gear"></i>
                                                </a>
                                                <div class="m-dropdown__wrapper">
                                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                    <div class="m-dropdown__inner">
                                                        <div class="m-dropdown__body">
                                                            <div class="m-dropdown__content">
                                                                <ul class="m-nav">
                                                                    <li class="m-nav__section m-nav__section--first">
                                                                                    <span class="m-nav__section-text">
                                                                                        Quick Actions
                                                                                    </span>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon flaticon-share"></i>
                                                                            <span class="m-nav__link-text">
                                                                                            Create Post
                                                                                        </span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                                            <span class="m-nav__link-text">
                                                                                            Send Messages
                                                                                        </span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon flaticon-multimedia-2"></i>
                                                                            <span class="m-nav__link-text">
                                                                                            Upload File
                                                                                        </span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__section">
                                                                                    <span class="m-nav__section-text">
                                                                                        Useful Links
                                                                                    </span>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon flaticon-info"></i>
                                                                            <span class="m-nav__link-text">
                                                                                            FAQ
                                                                                        </span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                                            <span class="m-nav__link-text">
                                                                                            Support
                                                                                        </span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__separator m-nav__separator--fit m--hide"></li>
                                                                    <li class="m-nav__item m--hide">
                                                                        <a href="#"
                                                                           class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
                                                                            Submit
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>-->
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="m_user_profile_tab_1">
                                    <form class="m-form m-form--fit m-form--label-align-right">
                                        <div class="m-portlet__body">
                                            <div class="form-group m-form__group m--margin-top-10 m--hide">
                                                <!--<div class="alert m-alert m-alert--default" role="alert">
                                                    The example form below demonstrates common HTML form elements that
                                                    receive updated styles from Bootstrap with additional classes.
                                                </div>-->
                                            </div>
                                                <?php

                                                //Loading Assets
                                                $asset_already = [];
                                                foreach($forms as $form) {
                                                $type = @$form['type'] ?: 'text';
                                                $name = $form['name'];

                                                if (in_array($type, $asset_already)) continue;
                                                ?>
                                                @if(env("APP_DEV",false) != true)
                                                    @if(file_exists(base_path('/vendor/cloudsourceit/crudbooster/src/views/default/type_components/'.$type.'/asset.blade.php')))
                                                        @include('crudbooster::default.type_components.'.$type.'.asset')
                                                    @elseif(file_exists(resource_path('views/vendor/crudbooster/type_components/'.$type.'/asset.blade.php')))
                                                        @include('vendor.crudbooster.type_components.'.$type.'.asset')
                                                    @endif
                                                @else
                                                    @if(file_exists(base_path('app/crudbooster/src/views/default/type_components/'.$type.'/asset.blade.php')))
                                                        @include('crudbooster::default.type_components.'.$type.'.asset')
                                                    @elseif(file_exists(resource_path('views/vendor/crudbooster/type_components/'.$type.'/asset.blade.php')))
                                                        @include('vendor.crudbooster.type_components.'.$type.'.asset')
                                                    @endif
                                                @endif
                                                <?php
                                                $asset_already[] = $type;
                                                }


                                                //Loading input components
                                                $header_group_class = "";
                                                foreach($forms as $index=>$form) {

                                                $name = $form['name'];
                                                @$join = $form['join'];
                                                @$value = (isset($form['value'])) ? $form['value'] : '';
                                                @$value = (isset($row->{$name})) ? $row->{$name} : $value;

                                                $old = old($name);
                                                $value = (!empty($old)) ? $old : $value;

                                                $validation = array();
                                                $validation_raw = isset($form['validation']) ? explode('|', $form['validation']) : array();
                                                if ($validation_raw) {
                                                    foreach ($validation_raw as $vr) {
                                                        $vr_a = explode(':', $vr);
                                                        if ($vr_a[1]) {
                                                            $key = $vr_a[0];
                                                            $validation[$key] = $vr_a[1];
                                                        } else {
                                                            $validation[$vr] = TRUE;
                                                        }
                                                    }
                                                }

                                                if (isset($form['callback_php'])) {
                                                    @eval("\$value = " . $form['callback_php'] . ";");
                                                }


                                                if (isset($form['callback'])) {
                                                    $value = call_user_func($form['callback'], $row);
                                                }

                                                if ($join && @$row) {
                                                    $join_arr = explode(',', $join);
                                                    array_walk($join_arr, 'trim');
                                                    $join_table = $join_arr[0];
                                                    $join_title = $join_arr[1];
                                                    $join_query_{$join_table} = DB::table($join_table)->select($join_title)->where("id", $row->{'id_' . $join_table})->first();
                                                    $value = @$join_query_{$join_table}->{$join_title};
                                                }
                                                $form['type'] = ($form['type']) ?: 'text';
                                                $type = @$form['type'];
                                                $required = (@$form['required']) ? "required" : "";
                                                $required = (@strpos($form['validation'], 'required') !== FALSE) ? "required" : $required;
                                                $readonly = (@$form['readonly']) ? "readonly" : "";
                                                $disabled = (@$form['disabled']) ? "disabled" : "";
                                                $placeholder = (@$form['placeholder']) ? "placeholder='" . $form['placeholder'] . "'" : "";
                                                $col_width = @$form['width'] ?: "col-sm-9";

                                                if ($parent_field == $name) {
                                                    $type = 'hidden';
                                                    $value = $parent_id;
                                                }

                                                if ($type == 'header') {
                                                    $header_group_class = "header-group-$index";
                                                } else {
                                                    $header_group_class = ($header_group_class) ?: "header-group-$index";
                                                }

                                                ?>

                                                @if(env("APP_DEV",false) != true)
                                                    @if(file_exists(base_path('/vendor/cloudsourceit/crudbooster/src/views/default/type_components/'.$type.'/component.blade.php')))
                                                        @include('crudbooster::default.type_components.'.$type.'.component')
                                                    @elseif(file_exists(resource_path('views/vendor/crudbooster/type_components/'.$type.'/component.blade.php')))
                                                        @include('vendor.crudbooster.type_components.'.$type.'.component')
                                                    @else
                                                        <p class='text-danger'>{{$type}} is not found in type
                                                            component
                                                            system</p><br/>
                                                    @endif
                                                @else
                                                    @if(file_exists(base_path('/app/crudbooster/src/views/default/type_components/'.$type.'/component.blade.php')))
                                                        @include('crudbooster::default.type_components.'.$type.'.component')
                                                    @elseif(file_exists(resource_path('views/vendor/crudbooster/type_components/'.$type.'/component.blade.php')))
                                                        @include('vendor.crudbooster.type_components.'.$type.'.component')
                                                    @else
                                                        <p class='text-danger'>{{$type}} is not found in type
                                                            component
                                                            system</p><br/>
                                                    @endif
                                                @endif
                                                <?php
                                                }
                                                ?>
                                            <!--<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-10 ml-auto">
                                                    <h3 class="m-form__section">
                                                        3. Social Links
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="example-text-input" class="col-2 col-form-label">
                                                    Linkedin
                                                </label>
                                                <div class="col-7">
                                                    <input class="form-control m-input" type="text"
                                                           value="www.linkedin.com/Mark.Andre">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="example-text-input" class="col-2 col-form-label">
                                                    Facebook
                                                </label>
                                                <div class="col-7">
                                                    <input class="form-control m-input" type="text"
                                                           value="www.facebook.com/Mark.Andre">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="example-text-input" class="col-2 col-form-label">
                                                    Twitter
                                                </label>
                                                <div class="col-7">
                                                    <input class="form-control m-input" type="text"
                                                           value="www.twitter.com/Mark.Andre">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="example-text-input" class="col-2 col-form-label">
                                                    Instagram
                                                </label>
                                                <div class="col-7">
                                                    <input class="form-control m-input" type="text"
                                                           value="www.instagram.com/Mark.Andre">
                                                </div>
                                            </div>-->
                                        </div>
                                        <div class="m-portlet__foot m-portlet__foot--fit">
                                            <br>

                                            <div class="m-form__actions">
                                                <div class="row">
                                                    <div class="col-2"></div>
                                                    <div class="col-7">
                                                        <button type="submit"
                                                                class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                            Guardar cambios
                                                        </button>
                                                        &nbsp;&nbsp;
                                                        <button type="reset"
                                                                class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                            Cancelar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--<div class="tab-pane " id="m_user_profile_tab_2"></div>
                                <div class="tab-pane " id="m_user_profile_tab_3"></div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        @endif
    </div>
    </div><!--END AUTO MARGIN-->

@endsection