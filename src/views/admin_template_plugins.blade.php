
<!-- REQUIRED JS SCRIPTS -->
<!--begin::Base Scripts -->
<script src="{{ asset("vendor/crudbooster/metronic/vendors/base/vendors.bundle.js") }}" type="text/javascript"></script>

<script src="{{asset('vendor/crudbooster/assets/sweetalert/dist/sweetalert.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('vendor/crudbooster/assets/sweetalert/dist/sweetalert.css')}}">

<script src="{{ asset("vendor/crudbooster/metronic/demo/default/base/scripts.bundle.js") }}" type="text/javascript"></script>
<!--end::Base Scripts -->
<!--begin::Page Vendors -->
<script src="{{ asset("vendor/crudbooster/metronic/vendors/custom/fullcalendar/fullcalendar.bundle.js") }}" type="text/javascript"></script>
<script src="{{ asset("vendor/crudbooster/metronic/demo/default/custom/components/forms/widgets/bootstrap-select.js") }}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset("vendor/crudbooster/metronic/app/js/dashboard.js") }}" type="text/javascript"></script>
<!--end::Page Snippets -->
